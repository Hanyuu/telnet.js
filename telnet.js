var net = require('net'),
	clients={},
	admins=[],
	exec=require('child_process').exec,
	p=console.log;
admins['rika']='nipa';

var server = net.createServer(function(c) { 
	var client;
	if(!clients[c.remoteAddress]){
		client=new clientObject(
			c,
			'connected',
			c.remoteAddress,
			'not-auth'
		); 
		clients[c.remoteAddress]=client;
	}else{
		client=clients[c.remoteAddress];
	}
	switch(client.state){
		case 'connected':
			c.write('Hello, enter login:\r\n');
			break;
		default:
			throw 'Unknown state: '+client.state;
	}
  
	clients[c.remoteAddress].data='';
  
	client.on('end', function() {
		console.log(client.ip+' '+client.user+' disconnected');
	});
  
	client.on('data',function(chunk){
		if(chunk.length==2&&chunk.toString().indexOf('\n')>0){
			var tmpData=clients[c.remoteAddress].data.trim();
			clients[c.remoteAddress].data='';
			switch(client.state){
				case 'connected':
					client.user=tmpData;
					client.state='req-password';
					client.send('Enter password for login '+client.user+':\r\n');
					break;
				case 'req-password':
					if(admins[client.user]==tmpData){
						client.send('Welcome '+client.user+'!\r\n');
						client.state='authorized';
						client.send(process.cwd()+'>');
					}else{
						client.send('Unknown login or password');
						client.close();
					}
					break;
				case 'authorized':
					if(tmpData=='exit'){
						client.send('Goodbay, '+client.user);
						client.close();
					}else if(/^cd(.+)$/.test(tmpData)){
						try{
							process.chdir(RegExp.$1.trim());
							client.chdir();
						}catch(err){
							client.send('Change dir failed: '+err);
						}
					}else{
						exec(tmpData,function(error, stdout, stderr){
							if(error){
								client.send('Error! '+error+'\r\n');
							}else{
								client.send(stdout);
								client.chdir();
							}
							client.chdir();
						});
					}
					break;
				default:
					throw 'Unknown state: '+client.state;
			}
		}else{
			clients[c.remoteAddress].data+=chunk.toString();
		}
	});
  
});

server.listen(8124);

function clientObject(socket,state,ip,user){

	this.socket=socket;
	this.state=state;
	this.ip=ip;
	this.user=user;
	this.data='';
	
	this.send=function(data){
		this.socket.write(data);
	};
	
	this.close=function(){
		delete clients[this.socket.remoteAddress]
		this.socket.end();
	};
	
	this.chdir=function(){
		this.socket.write(process.cwd()+'>\r');
	}
	
	this.on=function(event,callback){
		this.socket.on(event,callback);
	};
}
p('Loaded');
